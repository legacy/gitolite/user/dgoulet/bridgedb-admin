# -*- coding: utf-8 -*-
#===============================================================================
# logrotate.conf
# ~~~~~~~~~~~~~~
# This file should live at:
# polyanthum.torproject.org:/srv/bridges.torproject.org/etc/logrotate.conf
#
# :authors: The Tor Project, Inc.
# :license: This file is freely distributed as part of BridgeDB, see LICENSE
#           for details.
# :copyright: (c) 2007-2013 The Tor Project, Inc.
# :version: 0.0.3
#===============================================================================
#
# CHANGELOG:
# ~~~~~~~~~~ 
# Changes in version 0.0.3 - 2019-09-05
#   * ADD logrotate config for bridgedb-metrics.log.
#
# Changes in version 0.0.2 - 2014-02-20
#   * CHANGE assignment logs to be compressed daily.
#
# Changes in version 0.0.1 - 2013-08-30
#   * ADD version of config file in use on ponticum.
#
#===============================================================================

/srv/bridges.torproject.org/log/server.log {
    missingok
    #daily
    size 100k
    create 0644
    rotate 7
    compress
    compressoptions "-8"
    dateext
    postrotate
        if [ -e /srv/bridges.torproject.org/run/flog.pid ] ; then
            kill -HUP "$(cat /srv/bridges.torproject.org/run/flog.pid)"
        fi
    endscript
}

/srv/bridges.torproject.org/log/bridgedb-metrics.log {
    missingok
    daily
    create 0644
    rotate 30
    compress
    compressoptions "-8"
    dateext
}
